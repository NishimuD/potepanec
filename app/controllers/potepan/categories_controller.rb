class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:taxon_id])
    @taxonomies = Spree::Taxonomy.includes(:root)
    @products = @taxon.products.includes(master: [:default_price, :images])
  end
end
