class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_RELATED_NUMBER = 4

  def show
    @product = Spree::Product.find(params[:id])
    @product_properties = @product.product_properties.includes(:property)
    @related_products = Spree::Product.related_products(@product).limit(MAX_DISPLAY_RELATED_NUMBER)
  end
end
