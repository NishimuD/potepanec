require 'rails_helper'

RSpec.feature "Products", type: :feature do
  feature "商品詳細ページ" do
    given!(:taxon) { create(:taxon) }
    given!(:product) { create(:product, taxons: [taxon]) }
    given!(:related_product) { create(:product, taxons: [taxon]) }
    given!(:not_related_product) { create(:product) }

    scenario "product show page" do
      visit potepan_product_path product.id
      expect(page).to have_title "Product Details"
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      product.option_types.each do |option_type|
        option_type.option_values.each do |option_value|
          expect(page).to have_content option_value.presentation
        end
      end
      within "div.productCaption" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
        expect(page).not_to have_content not_related_product.name
      end
      within "div.productBox" do
        expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
      end
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(related_product.taxons.first.id)
    end
  end
end
