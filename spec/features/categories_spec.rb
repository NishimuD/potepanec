require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  feature "商品カテゴリーページ" do
    given!(:taxonomy) { create(:taxonomy) }
    given!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    given!(:product) { create(:product, taxons: [taxon]) }

    scenario "category categories_show page" do
      visit potepan_category_path(taxon.id)
      expect(page).to have_title "Categories"
      within "div.sideBar" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.name
      end
      within "div.productCaption" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
      end
      expect(page).to have_link product.name, href: potepan_product_path(product.id)
    end
  end
end
