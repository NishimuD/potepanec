require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    render_views
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_properties) { create(:product_property, product: product) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
    let!(:not_related_products) { create_list(:product, 4) }
    let!(:related_products_5) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'response code 200' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns product with given id' do
      expect(response.body).to include product.name
    end

    it 'renders the :show template' do
      expect(response).to render_template(:show)
    end

    it 'check @product' do
      expect(assigns(:product)).to eq product
    end

    it 'check @product_properties' do
      expect(assigns(:product_properties)).to eq product.product_properties
    end

    it "assigns array @related_products" do
      expect(assigns(:related_products)).to match_array related_products
    end

    it 'check @related_product longth' do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
