require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { taxon_id: taxon.id }
    end

    it 'response code 200' do
      expect(response).to have_http_status(:ok)
    end

    it 'renders the :show template' do
      expect(response).to render_template(:show)
    end

    it 'check @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'check @taxonomies' do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it "check @products" do
      expect(assigns(:products)).to match_array product
    end
  end
end
